//
//  LayerPhotoViewController.swift
//  Filterer
//
//  Created by Radja on 10.01.17.
//  Copyright © 2017 UofT. All rights reserved.
//

import UIKit

class LayerPhotoViewController: UIViewController, UIScrollViewDelegate
{

    var image: UIImage!
     var modifiedImage: UIImage!
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet var bottomMenu: UIView!
    @IBOutlet weak var socialButton: UIButton!
    @IBOutlet weak var textbox: UITextField!
  
    
    override func viewDidLoad() {
        super.viewDidLoad()
        CreateBottomMenu()
        socialButton.setTitle("\u{f061}", forState: .Normal)

        imageView.image = image;
        
    }
    
    func CreateBottomMenu(){
    
        bottomMenu.backgroundColor = UIColor.lightGrayColor().colorWithAlphaComponent(0.5)
        bottomMenu.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(bottomMenu)
        let bottomConstraint = bottomMenu.bottomAnchor.constraintEqualToAnchor(view.bottomAnchor)
        let leftConstraint = bottomMenu.leftAnchor.constraintEqualToAnchor(view.leftAnchor)
        let rightConstraint = bottomMenu.rightAnchor.constraintEqualToAnchor(view.rightAnchor)
        let heightConstraint = bottomMenu.heightAnchor.constraintEqualToConstant(44)
        
        NSLayoutConstraint.activateConstraints([bottomConstraint, leftConstraint, rightConstraint, heightConstraint])
        view.layoutIfNeeded()

    }
    
    @IBAction func onSocial(sender: UIButton) {
        NSUserDefaults.standardUserDefaults().setObject(UIImagePNGRepresentation(modifiedImage), forKey: "modifiedImage")
    }
    
    @IBAction func onEditingChanged(sender: UITextField) {
             modifiedImage = addTextToImage(textbox.text!, inImage: image!, atPoint: CGPointMake(1, 2))
        imageView.image = modifiedImage
        
       //
        
        
    }
    
    func addTextToImage(drawText: NSString, inImage: UIImage, atPoint:CGPoint)->UIImage{
        
        // Setup the font specific variables
        let textColor: UIColor = UIColor.lightGrayColor()
        // calculate the overlay font size, otherwise for high-res images it will be too small,
        // and for low-res images it will be too huge.
        let fontSize: Int = Int(floorf(Float(inImage.size.width) / 5.0))
        let textFont: UIFont = UIFont(name: "Helvetica Bold", size: CGFloat(fontSize))!
        
        //Setup the image context using the passed image.
        UIGraphicsBeginImageContext(inImage.size)
        
        //Setups up the font attributes that will be later used to dictate how the text should be drawn
        let textFontAttributes = [
            NSFontAttributeName: textFont,
            NSForegroundColorAttributeName: textColor,
            
        ]
        
        //Put the image into a rectangle as large as the original image.
        inImage.drawInRect(CGRectMake(0, 0, inImage.size.width, inImage.size.height))
        
        // Creating a point within the space that is as bit as the image.
        let rect: CGRect = CGRectMake(atPoint.x, atPoint.y, inImage.size.width, inImage.size.height)
        
        //Now Draw the text into an image.
        drawText.drawInRect(rect, withAttributes: textFontAttributes)
        
        // Create a new image out of the images we have created
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()
        
        // End the context now that we have the image we need
        UIGraphicsEndImageContext()
        
        //And pass it back up to the caller.
        return newImage
        
    }
    
    func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView? {
        return imageView
    }



}
