//
//  ViewController.swift
//  Filterer
//
//  Created by Jack on 2015-09-22.
//  Copyright © 2015 UofT. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIScrollViewDelegate {

    var filteredImage: UIImage?
    var originalImage: UIImage?
    var formerImage: UIImage?
    var overlay: UIView!
    var currentFilter = FilterType.none

    @IBOutlet var imageView: UIImageView!
    @IBOutlet var secondaryMenu: UIView!
    @IBOutlet var bottomMenu: UIView!
    @IBOutlet weak var slider: UISlider!
    @IBOutlet var sliderMenu: UIView!
    @IBOutlet weak var compareButton: UIButton!
    @IBOutlet var filterButton: UIButton!
    
    @IBOutlet   var undoButton: UIButton!     
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var redFilterButton: UIButton!
    @IBOutlet weak var greenFilterButton: UIButton!
    @IBOutlet weak var blueFilterButton: UIButton!
    @IBOutlet weak var yellowFilterButton: UIButton!
    @IBOutlet weak var purpleFilterButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    
    @IBOutlet var zoomTapGestureRecognizer: UITapGestureRecognizer!
    
    var iProcessor = ImageProcessor()
 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        CreateSliderMenu()
        CreateSecondaryMenu()
                originalImage = self.imageView.image
        
        self.navigationController!.navigationBar.barTintColor = UIColor.darkGrayColor()
        self.bottomMenu.backgroundColor = UIColor.darkGrayColor()
        
       
        zoomTapGestureRecognizer.numberOfTapsRequired = 2
        
        //Mark: Set FontAwesome Icon
         undoButton.setTitle("\u{f0e2}", forState: UIControlState.Normal)
         nextButton.setTitle("\u{f061}", forState: UIControlState.Normal)
        
    }
    
    //MARK: TapGestureRecognizer - if image is tapped this method is triggered
    @IBAction func onZoomTapGesture(sender: UITapGestureRecognizer) {
        scrollView.zoomScale = 1.5 * scrollView.zoomScale
    }
    
    @IBAction func onUndo(sender: UIButton) {
        imageView.image = originalImage
    }
    
    func CreateSecondaryMenu(){
        secondaryMenu.backgroundColor = UIColor.lightGrayColor().colorWithAlphaComponent(0.5)
        secondaryMenu.translatesAutoresizingMaskIntoConstraints = false
    }
    
    func CreateSliderMenu(){
        sliderMenu.backgroundColor = UIColor.lightGrayColor().colorWithAlphaComponent(0.5)
        sliderMenu.translatesAutoresizingMaskIntoConstraints = false
    }

    // MARK: Share
    @IBAction func onShare(sender: AnyObject) {
        let activityController = UIActivityViewController(activityItems: ["Check out our really cool app", imageView.image!], applicationActivities: nil)
        presentViewController(activityController, animated: true, completion: nil)
    }
    
    // MARK: New Photo
    @IBAction func onNewPhoto(sender: AnyObject) {
        let actionSheet = UIAlertController(title: "New Photo", message: nil, preferredStyle: .ActionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .Default, handler: { action in
            self.showCamera()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Album", style: .Default, handler: { action in
            self.showAlbum()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: nil))
        
        self.presentViewController(actionSheet, animated: true, completion: nil)
    }
    
    @IBAction func onRedFilter(sender: UIButton) {
        openSliderMenu(sender)
        currentFilter = FilterType.red
        applyFilter(currentFilter, percentage: 35.00)
    }
    
    @IBAction func onGreenFilter(sender: UIButton) {
        openSliderMenu(sender)
        currentFilter = FilterType.green
        applyFilter(currentFilter, percentage: 35.00)
    }
    
    @IBAction func onBlueFilter(sender: UIButton) {
        openSliderMenu(sender)
        currentFilter = FilterType.blue
        applyFilter(currentFilter, percentage: 35.00)
    }
    
    @IBAction func onYellowFilter(sender: UIButton) {
        openSliderMenu(sender)
        currentFilter = FilterType.yellow
        applyFilter(currentFilter, percentage: 35.00)
    }
    
    @IBAction func onPurpleFilter(sender: UIButton) {
        openSliderMenu(sender)
        currentFilter = FilterType.viollet
        applyFilter(currentFilter, percentage: 35.00)
    }
    
    func openSliderMenu(sender: UIButton){
        if (sender.selected) {
            hideSliderMenu()
            sender.selected = false
        } else {
            showSliderMenu()
            sender.selected = true
        }
    }
    
    @IBAction func onCompare(sender: UIButton) {
        if (sender.selected) {
            imageView.image = filteredImage
            sender.selected = false
        } else {
            imageView.image = originalImage
            sender.selected = true
        }
    }
    
    func showSliderMenu(){
        
        view.addSubview(sliderMenu)
        let bottomConstraint = sliderMenu.bottomAnchor.constraintEqualToAnchor(secondaryMenu.topAnchor)
        let leftConstraint = sliderMenu.leftAnchor.constraintEqualToAnchor(view.leftAnchor)
        let rightConstraint = sliderMenu.rightAnchor.constraintEqualToAnchor(view.rightAnchor)
        let heightConstraint = sliderMenu.heightAnchor.constraintEqualToConstant(44)
        
        NSLayoutConstraint.activateConstraints([bottomConstraint, leftConstraint, rightConstraint, heightConstraint])
        view.layoutIfNeeded()
      
        //MARK: animation
        self.sliderMenu.alpha = 0
        UIView.animateWithDuration(0.4) {
            self.sliderMenu.alpha = 1.0
        }
    }
    
    func hideSliderMenu(){
        UIView.animateWithDuration(0.4, animations: {
            self.sliderMenu.alpha = 0
        }) { completed in
            if completed == true {
                self.sliderMenu.removeFromSuperview()
            }
        }
    }
    
    //MARK: onSlide triggered by slider
    @IBAction func onSlide(sender: UISlider) {
        let sliderValue = self.slider.value;
        switch currentFilter {
        case .red:
            applyFilter(currentFilter, percentage:  Double(sliderValue))
        case .yellow:
            applyFilter(currentFilter, percentage:  Double(sliderValue))
        case .blue:
            applyFilter(currentFilter, percentage:  Double(sliderValue))
        case .green:
            applyFilter(currentFilter, percentage:  Double(sliderValue))
        default:
            applyFilter(currentFilter, percentage:  Double(sliderValue))
        }
       
    }
    
    func applyFilter(filter: FilterType, percentage:Double)
    {
        formerImage = imageView.image
        iProcessor.setImage(originalImage!)
        filteredImage =  iProcessor.filter(filter , PercentageOfImpact: percentage)
        imageView.image = filteredImage
    }
    
    func showCamera() {
        let cameraPicker = UIImagePickerController()
        cameraPicker.delegate = self
        cameraPicker.sourceType = .Camera
        
        presentViewController(cameraPicker, animated: true, completion: nil)
    }
    
    func showAlbum() {
        let cameraPicker = UIImagePickerController()
        cameraPicker.delegate = self
        cameraPicker.sourceType = .PhotoLibrary
        
        presentViewController(cameraPicker, animated: true, completion: nil)
    }
    
    // MARK: UIImagePickerControllerDelegate
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        dismissViewControllerAnimated(true, completion: nil)
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            imageView.image = image
        }
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    // MARK: Filter Menu
    @IBAction func onFilter(sender: UIButton) {
        if (sender.selected) {
            hideSecondaryMenu()
            sender.selected = false
        } else {
            showSecondaryMenu()
            sender.selected = true
        }
    }
    
    func showSecondaryMenu() {
        view.addSubview(secondaryMenu)
        
        let bottomConstraint = secondaryMenu.bottomAnchor.constraintEqualToAnchor(bottomMenu.topAnchor)
        let leftConstraint = secondaryMenu.leftAnchor.constraintEqualToAnchor(view.leftAnchor)
        let rightConstraint = secondaryMenu.rightAnchor.constraintEqualToAnchor(view.rightAnchor)
        
        let heightConstraint = secondaryMenu.heightAnchor.constraintEqualToConstant(44)
        
        NSLayoutConstraint.activateConstraints([bottomConstraint, leftConstraint, rightConstraint, heightConstraint])
        
        view.layoutIfNeeded()
        
        self.secondaryMenu.alpha = 0
        UIView.animateWithDuration(0.4) {
            self.secondaryMenu.alpha = 1.0
        }
    }

    func hideSecondaryMenu() {
        UIView.animateWithDuration(0.4, animations: {
            self.secondaryMenu.alpha = 0
            }) { completed in
                if completed == true {
                    self.secondaryMenu.removeFromSuperview()
                }
        }
    }

    // MARK: UIScrollViewDelegate
    func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView? {
        return imageView
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "layerPhoto"{
            let destinationController = segue.destinationViewController as! LayerPhotoViewController
            destinationController.image = imageView.image
        
        }
    }
}

