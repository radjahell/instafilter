//
//  ImageProcessor.swift
//  Filterer
//
//  Created by Radim on 12/12/2016.
//  Copyright © 2016 UofT. All rights reserved.
//

import UIKit

class ImageProcessor
{
    var image = UIImage();
    
    func setImage(img:UIImage){
        image = img
    }
    
    
    func filter(filterName:FilterType ,PercentageOfImpact:Double)  -> UIImage {
        
        var myRGBA = RGBAImage(image: image)!
        
        // RED FILTER   
        func RedFilter(percent:Double = 10){
            var percentCalc = max(0, min(100, percent))
            percentCalc = (100 + percentCalc)/100
            for y in 0..<myRGBA.height {
                for x in 0..<myRGBA.width {
                    let index = y * myRGBA.width + x
                    var pixel = myRGBA.pixels[index]
                    pixel.red = UInt8(max(0, min(255, Double(pixel.red) * percentCalc)))
                    pixel.blue = 0
                    pixel.green = 0
                    myRGBA.pixels[index] = pixel
                }
            }
        }
        
        // BLUE FILTER
        func BlueFilter(percent:Double = 10){
            var percentCalc = max(0, min(100, percent))
            percentCalc = (100 + percentCalc)/100
            for y in 0..<myRGBA.height {
                for x in 0..<myRGBA.width {
                    let index = y * myRGBA.width + x
                    var pixel = myRGBA.pixels[index]
                    pixel.blue = UInt8(max(min(255, Double(pixel.blue) * percentCalc), 0))
                    pixel.green = 0
                    pixel.red = 0
                    myRGBA.pixels[index] = pixel
                    
                }
            }
        }
        
        // GREEN FILTER
        func GreenFilter(percent:Double = 10){
            var percentCalc = max(0, min(100, percent))
            percentCalc = (100 + percentCalc)/100
            for y in 0..<myRGBA.height {
                for x in 0..<myRGBA.width {
                    let index = y * myRGBA.width + x
                    var pixel = myRGBA.pixels[index]
                    pixel.red = 0
                    pixel.green = UInt8(max(min(255, Double(pixel.green) * percentCalc), 0))
                    pixel.blue = 0
                    myRGBA.pixels[index] = pixel
                }
            }
        }
        
        // YELLOW FILTER
                func YellowFilter(percent:Double = 10){
            var percentCalc = max(0, min(100, percent))
            percentCalc = (100 + percentCalc)/100
            for y in 0..<myRGBA.height {
                for x in 0..<myRGBA.width {
                    let index = y * myRGBA.width + x
                    var pixel = myRGBA.pixels[index]
                    pixel.blue = 20 * UInt8(percentCalc)
                    myRGBA.pixels[index] = pixel
                }
            }
        }
        
        // VIOLET FILTER
               func VioletFilter(percent:Double = 10){
            var percentCalc = max(0, min(100, percent))
            percentCalc = (100 + percentCalc)/100
            for y in 0..<myRGBA.height {
                for x in 0..<myRGBA.width {
                    let index = y * myRGBA.width + x
                    var pixel = myRGBA.pixels[index]
                    let avgRed = 119
                    let avgGreen = 98
                    let avgBlue = 83
                    let redDiff = Int(pixel.red) - avgRed
                    let greenDiff = Int(pixel.green) - avgGreen
                    let blueDiff = Int(pixel.blue) - avgBlue
                    pixel.red = UInt8(max(min(255, redDiff + 2 + Int(percentCalc)), 0))
                    pixel.blue = UInt8(max(min(255, avgBlue + 12 * blueDiff * Int(percentCalc)), 0))
                    pixel.green = UInt8(max(min(255, avgGreen + 1 * greenDiff * Int(percentCalc)), 0))
                    myRGBA.pixels[index] = pixel
                }
            }
        }
        
        
        switch filterName
        {
        case .red:RedFilter(Double(PercentageOfImpact))
            
        case .blue:BlueFilter(Double(PercentageOfImpact))
            
        case .green:GreenFilter(Double(PercentageOfImpact))
            
        case .yellow:YellowFilter(Double(PercentageOfImpact))
            
        case .viollet:VioletFilter(Double(PercentageOfImpact))
        default:print("Sample one")
           
        }
        
        let finished = myRGBA.toUIImage()!
        return (finished)
    }
}
